package rca.stock.honorine.stocksoap.endpoint;

import honorine.stock.rca.items.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import rca.stock.honorine.stocksoap.bean.Item;
import rca.stock.honorine.stocksoap.bean.Supplier;
import rca.stock.honorine.stocksoap.repository.ItemRepository;
import rca.stock.honorine.stocksoap.repository.SupplierRepository;

import java.util.List;
@Endpoint
public class ItemEndpoint {
    @Autowired
    private ItemRepository itemRepository;

    @Autowired
    private SupplierRepository supplierRepository;

    @PayloadRoot(namespace = "http://rca.stock.honorine/items",localPart = "GetItemDetails")
    @ResponsePayload
    public GetItemDetailsResponse findItem(@RequestPayload GetItemDetailsRequest request)
    {
        Item item= itemRepository.findById(request.getId()).get();
        GetItemDetailsResponse itemDetailsResponse = mapItemDetails(item);
        return itemDetailsResponse;
    }



    @PayloadRoot(namespace = "http://rca.stock.honorine/items",localPart = "GetAllItemDetailsRequest")
    @ResponsePayload

    public GetAllItemDetailsResponse getAllItems(@RequestPayload GetAllItemDetailsRequest request)
    {
        GetAllItemDetailsResponse itemResp = new GetAllItemDetailsResponse();
        System.out.println("Reached here");
        List<Item> items = itemRepository.findAll();
        System.out.println("List: "+ items);

        for(Item item: items){
            GetItemDetailsResponse ItemDetailsResponse = mapItemDetails(item);
            itemResp.getItemDetails().add(ItemDetailsResponse.getItemDetails());
        }

        return  itemResp;
    }



    @PayloadRoot(namespace = "http://rca.stock.honorine/items", localPart = "CreateItemDetailsRequest")
    @ResponsePayload
    public CreateItemDetailsResponse save(@RequestPayload CreateItemDetailsRequest request) {
        Supplier course = supplierRepository.findById(request.getItemDetails().getId()).get();

        Item testStudent = itemRepository.save(new Item(
                request.getItemDetails().getId(),
                request.getItemDetails().getName(),
                request.getItemDetails().getItemCode(),
                request.getItemDetails().getStatus(),
                request.getItemDetails().getPrice(),
                course
        ));

        System.out.println("Test: "+testStudent);

//        System.out.println("course details "+ course);
        CreateItemDetailsResponse ItemDetailsResponse = new CreateItemDetailsResponse();
        ItemDetailsResponse.setItemDetails(request.getItemDetails());
        ItemDetailsResponse.setMessage("Created Successfully");
        return ItemDetailsResponse;
    }


    @PayloadRoot(namespace = "http://rca.stock.honorine/items", localPart = "DeleteItem")
    @ResponsePayload
    public DeleteItemDetailsResponse delete(@RequestPayload DeleteItemDetailsRequest request) {

        System.out.println("ID: "+request.getId());
        itemRepository.deleteById(request.getId());
        DeleteItemDetailsResponse ItemDetailsResponse = new DeleteItemDetailsResponse();
        ItemDetailsResponse.setMessage("Deleted Successfully");
        return ItemDetailsResponse;
    }



    private GetItemDetailsResponse mapItemDetails(Item student){
        ItemDetails details = mapItem(student);
        GetItemDetailsResponse ItemDetailsResponse = new GetItemDetailsResponse();
        ItemDetailsResponse.setItemDetails(details);
        return ItemDetailsResponse;
    }

    private UpdateItemDetailsResponse mapUpdateItemDetails(Item std, String message) {
        ItemDetails details = mapItem(std);
        UpdateItemDetailsResponse resp = new UpdateItemDetailsResponse();
        resp.setItemDetails(details);
        resp.setMessage(message);
        return resp;
    }

    private ItemDetails mapItem(Item item){
        ItemDetails itemDetails = new ItemDetails();
        itemDetails.setId(item.getId());
        itemDetails.setName(item.getName());
        itemDetails.setStatus(item.getStatus());
        itemDetails.setSupplier(String.valueOf(item.getSupplier().getId()));
        return itemDetails;
    }
}
