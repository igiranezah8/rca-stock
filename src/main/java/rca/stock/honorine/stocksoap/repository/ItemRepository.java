package rca.stock.honorine.stocksoap.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import rca.stock.honorine.stocksoap.bean.Item;
@Repository
public interface ItemRepository extends JpaRepository<Item, Integer> {
}
