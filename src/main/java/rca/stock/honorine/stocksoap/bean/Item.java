package rca.stock.honorine.stocksoap.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Item {
    @Id
    @GeneratedValue
    private int id;

    private String name;

    private String itemCode;

    private String status;

    private int price;

    @OneToOne
    private Supplier supplier;

    @Override
    public String toString() {
        return String.format("Item [id=%s, name=%s, itemCode=%s, status=%s, price=%s, supplier=%s]", id, name, itemCode, status, price, supplier);
    }

}
