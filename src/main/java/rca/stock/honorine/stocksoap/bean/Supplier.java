package rca.stock.honorine.stocksoap.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Supplier {
    @Id
    @GeneratedValue
    private int id;

    private String names;

    private String email;

    private String mobile;

    @Override
    public String toString() {
        return String.format("Supplier [id=%s, names=%s, email=%s, mobile=%s]", id, names, email, mobile);
    }
}
