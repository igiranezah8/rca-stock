package rca.stock.honorine;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HonorineApplication {

    public static void main(String[] args) {
        SpringApplication.run(HonorineApplication.class, args);
    }

}
